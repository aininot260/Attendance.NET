﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class course_del : System.Web.UI.Page
{
    DBOP Query = new DBOP();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("course_add.aspx");
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("course_edit.aspx");
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("course_del.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("course_info.aspx");
    }

    protected void Button7_Click(object sender, EventArgs e)
    {
        Query.course_del(TextBox8.Text);
        Response.Write("<script>alert('删除成功！')</script>");
    }
}