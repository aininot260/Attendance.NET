﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class course_edit : System.Web.UI.Page
{
    DBOP Query = new DBOP();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("course_add.aspx");
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("course_edit.aspx");
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("course_del.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("course_info.aspx");
    }

    protected void Button5_Click(object sender, EventArgs e)
    {
        Query.course_edit(TextBox8.Text, TextBox13.Text, TextBox9.Text, TextBox14.Text, TextBox10.Text, TextBox15.Text, TextBox11.Text, TextBox16.Text, TextBox12.Text, TextBox17.Text);
        Response.Write("<script>alert('修改成功！')</script>");
    }
}