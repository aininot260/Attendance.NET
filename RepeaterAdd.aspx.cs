﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RepeaterAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //数据源
            List<Info> obj = new List<Info>();
            Info myinfo;
            for (int i = 1; i < 6; i++)
            {
                myinfo = new Info();
                myinfo.Keys = "欢迎" + i.ToString();
                myinfo.Values = "上海欢迎您..." + i.ToString();
                obj.Add(myinfo);
            }
            Repeater1.DataSource = obj;
            Repeater1.DataBind();
        }
    }

    /// <summary>
    /// 批量添加保存key-value
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem Item in Repeater1.Items)
        {
            TextBox tbKeys = (TextBox)Item.FindControl("TextBox1");
            TextBox tbValuse = (TextBox)Item.FindControl("TextBox2");
            DropDownList ddlIsUse = (DropDownList)Item.FindControl("DropDownList1");
            
            Response.Write("<b>keys:</b>" + tbKeys.Text + "   <b>Values:</b>" + tbValuse.Text + "   <b>是否有效:</b>" + ddlIsUse.SelectedItem.Text);
            Response.Write("<br/>");
            //这里可以改成写入数据库...
        }

    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }
}

/// <summary>
/// 信息类
/// </summary>
public class Info
{
    private string _keys;
    private string _values;

    /// <summary>
    /// 名字
    /// </summary>
    public string Keys
    {
        get
        {
            return this._keys;
        }
        set
        {
            this._keys = value;
        }

    }

    /// <summary>
    /// 值
    /// </summary>
    public string Values
    {
        get
        {

            return this._values;
        }
        set
        {
            this._values = value;
        }
    }
}