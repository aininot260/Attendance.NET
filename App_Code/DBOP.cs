﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
public class DBOP
{
    public static string connstr = @"server=47.94.193.149;uid=db_user;pwd=1234567890Bb;database=attendance;charset=utf8;port=3306";
    MySqlConnection Conn = new MySqlConnection(connstr);
    public DataSet student_info()
    {
        DataSet ds = new DataSet();
        string sql = "Select * From Student";
        MySqlDataAdapter da = new MySqlDataAdapter(sql,Conn);
        try
        {
            ///读取数据
            da.Fill(ds);
        }
        finally
        {
            if (Conn != null)
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }
            }
        }
        return ds;
    }
    public DataSet teacher_info()
    {
        DataSet ds = new DataSet();
        string sql = "Select * From Teacher";
        MySqlDataAdapter da = new MySqlDataAdapter(sql, Conn);
        try
        {
            ///读取数据
            da.Fill(ds);
        }
        finally
        {
            if (Conn != null)
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }
            }
        }
        return ds;
    }
    public DataSet course_info()
    {
        DataSet ds = new DataSet();
        string sql = "Select * From Course";
        MySqlDataAdapter da = new MySqlDataAdapter(sql, Conn);
        try
        {
            ///读取数据
            da.Fill(ds);
        }
        finally
        {
            if (Conn != null)
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }
            }
        }
        return ds;
    }
    public DataSet class_info()
    {
        DataSet ds = new DataSet();
        string sql = "Select * From Class";
        MySqlDataAdapter da = new MySqlDataAdapter(sql, Conn);
        try
        {
            ///读取数据
            da.Fill(ds);
        }
        finally
        {
            if (Conn != null)
            {
                if (Conn.State == ConnectionState.Open)
                {
                    Conn.Close();
                }
            }
        }
        return ds;
    }
    public void student_add(string s_Id,string s_Name,string s_Class)
    {
        string sql = "Insert Into Student (s_Id,s_Name,s_Class) Values ('" + s_Id + "','" + s_Name + "','" + s_Class + "')";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void teacher_add(string t_Id, string t_Name, string t_Password,string t_Uuid)
    {
        string sql = "Insert Into Teacher (t_Id,t_Name,t_Password,t_Uuid) Values ('" + t_Id + "','" + t_Name + "','" + t_Password + "','" + t_Uuid + "')";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void class_add(string class_Name, string s_Class, string Total)
    {
        string sql = "Insert Into Class (class_Name,s_Class,Total) Values ('" + class_Name + "','" + s_Class + "','" + Total + "')";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void course_add(string c_Id, string t_Id, string s_Class,string c_Name,string c_startDay,string c_endDay,string c_Time,string c_Week,string Permission,string c_Classroom)
    {
        string sql = "Insert Into Course (c_Id,t_Id,s_Class,c_Name,c_startDay,c_endDay,c_Time,c_Week,Permission,c_Classroom) Values ('" + c_Id + "','" + t_Id + "','" + s_Class + "','" + c_Name + "','" + c_startDay + "','" + c_endDay + "','" + c_Time + "','" + c_Week + "','" + Permission + "','" + c_Classroom + "')";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void student_del(string s_Id)
    {
        string sql = "Delete From Student Where s_Id='" + s_Id + "'";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void teacher_del(string t_Id)
    {
        string sql = "Delete From Teacher Where t_Id='" + t_Id + "'";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void course_del(string c_Id)
    {
        string sql = "Delete From Course Where c_Id='" + c_Id + "'";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void class_del(string s_Class)
    {
        string sql = "Delete From Class Where s_Class='" + s_Class + "'";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void student_edit(string s_Id,string s_Name,string s_Class)
    {
        string sql = "Update Student Set s_Name='" + s_Name + "' And s_Class='" + s_Class + "' Where s_Id='" + s_Id + "'";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void teacher_edit(string t_Id, string t_Name, string t_Password, string t_Uuid)
    {
        string sql = "Update Teacher Set t_Name='" + t_Name + "' And t_Password='" + t_Password + "' And t_Uuid='" + t_Uuid + "' Where t_Id='" + t_Id + "'";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void class_edit(string class_Name, string s_Class, string Total)
    {
        string sql = "Update Class Set class_Name='" + class_Name + "' And Total='" + Total + "' Where s_Class='" + s_Class + "'";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
    public void course_edit(string c_Id, string t_Id, string s_Class, string c_Name, string c_startDay, string c_endDay, string c_Time, string c_Week, string Permission, string c_Classroom)
    {
        string sql = "Update Course Set t_Id='" + t_Id + "' And s_Class='" + s_Class + "' And c_Name='" + c_Name + "' And c_startDay='" + c_startDay + "' And c_endDay='" + c_endDay + "' And c_Time='" + c_Time + "' And c_Week='" + c_Week + "' And Permission='" + Permission + "' And c_Classroom='" + c_Classroom + "' Where c_Id='" + c_Id + "'";
        try
        {
            MySqlCommand com = new MySqlCommand(sql, Conn);
            Conn.Open();
            com.ExecuteReader();
        }
        finally
        {
            Conn.Close();
        }
    }
}