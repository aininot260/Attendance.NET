﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>燕山大学签到管理系统</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="robots" content="" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0"/>
	<link rel="stylesheet" href="css/style.css" media="all" />
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" media="all" /><![endif]-->
	<!--[if lt IE 9]><link rel="stylesheet" href="css/lt-ie-9.css" media="all" /><![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="testing">
<header class="main">
	<h1>燕山大学签到管理系统</h1>
</header>
<section class="user">
	<div class="profile-img">
		<p>欢迎使用！</p>
	</div>
	<div class="buttons">
		<span class="button blue"><a href="logout.aspx">Logout</a></span>
	</div>
</section>
</div>
<nav>
	<ul>
		<li class="section"><a href="dashboard.aspx"><span class="icon">&#128200;</span>基本信息</a></li>
		<li><a href="teacher.aspx"><span class="icon">&#128100;</span>教师信息管理</a></li>
		<li><a href="student.aspx"><span class="icon">&#128101;</span>学生信息管理</a></li>
        <li><a href="course.aspx"><span class="icon">&#128197;</span>课程管理</a></li>
		<li><a href="class.aspx"><span class="icon">&#127891;</span>班级管理</a></li>
	</ul>
</nav>

<section class="alert">
	<div class="green">	
		<p>在此处可以添加使用说明</p>
	</div>
</section>
<section class="content">
	<section class="widget">
		<header>
			<span class="icon">&#128200;</span>
			<hgroup>
				<h1>基本信息</h1>
				<h2>副标题</h2>
			</hgroup>
		</header>
		<div class="content cycle">
            			<div id="flot-example-1" class="graph-area">
                          <iframe name="ad" src="dashboard_info.aspx" width="1300" height="600" ></iframe>
            			</div>
		</div>
        	</section>
	<div class="widget-container">
		<div style="height:80px">
		Collect from <a href="http://www.cssmoban.com/" title="网页模板" target="_blank">网页模板</a> - More Templates <a href="http://www.cssmoban.com/" target="_blank" title="模板之家">模板之家</a>
		</div>
	</div>
</section>
    </div>
    </form>
</body>
</html>
