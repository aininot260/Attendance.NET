﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="class_info.aspx.cs" Inherits="class_info" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
                    <br />
            <asp:Button ID="Button1" runat="server" Text="添加" OnClick="Button1_Click" />
            &nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button2" runat="server" Text="修改" OnClick="Button2_Click" />
&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button3" runat="server" Text="删除" OnClick="Button3_Click" />
            &nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button4" runat="server" Text="查询" OnClick="Button4_Click"/>
            <br />
            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" SkinID="GridView" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" PageSize="15" Width="1260px">
                <Columns>
                    <asp:BoundField DataField="class_Name" HeaderText="班级名"></asp:BoundField>
                    <asp:BoundField DataField="s_Class" HeaderText="班级编号"></asp:BoundField>
                    <asp:BoundField DataField="Total" HeaderText="班级总人数"></asp:BoundField>
                </Columns>
            </asp:GridView>
    </div>
    </form>
</body>
</html>
