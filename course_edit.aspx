﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="course_edit.aspx.cs" Inherits="course_edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
                 <br />
            <asp:Button ID="Button1" runat="server" Text="添加" OnClick="Button1_Click" Enabled="False"/>
            &nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button2" runat="server" Text="修改" OnClick="Button2_Click"/>
&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button3" runat="server" Text="删除" OnClick="Button3_Click"/>
        &nbsp;&nbsp;&nbsp;
             <asp:Button ID="Button4" runat="server" Text="查询" OnClick="Button4_Click"/>
            <br />
                 <br />
                 <asp:Label ID="Label8" runat="server" Text="课程标号："></asp:Label>
                 <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;
                 <asp:Label ID="Label13" runat="server" Text="教师工号："></asp:Label>
                 <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox>
                 <br />
                 <br />
                 <asp:Label ID="Label9" runat="server" Text="上课班级："></asp:Label>
                 <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;
                 <asp:Label ID="Label14" runat="server" Text="课程名称："></asp:Label>
                 <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox>
                 <br />
                 <br />
                 <asp:Label ID="Label10" runat="server" Text="起始周："></asp:Label>
                 <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;
                 <asp:Label ID="Label15" runat="server" Text="结束周："></asp:Label>
                 <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox>
                 <br />
                 <br />
                 <asp:Label ID="Label11" runat="server" Text="上课时间："></asp:Label>
                 <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;
                 <asp:Label ID="Label16" runat="server" Text="上课星期："></asp:Label>
                 <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox>
                 <br />
                 <br />
                 <asp:Label ID="Label12" runat="server" Text="是否开课："></asp:Label>
                 <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;
                 <asp:Label ID="Label17" runat="server" Text="上课教室："></asp:Label>
                 <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox>
                 <br />
                 <br />
                 <asp:Button ID="Button5" runat="server" OnClick="Button5_Click" Text="提交" />
            <br />
    </div>
    </form>
</body>
</html>
