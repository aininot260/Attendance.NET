﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class teacher_add : System.Web.UI.Page
{
    DBOP Query = new DBOP();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("teacher_add.aspx");
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("teacher_edit.aspx");
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("teacher_del.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("teacher_info.aspx");
    }

    protected void Button5_Click(object sender, EventArgs e)
    {
        Query.teacher_add(TextBox1.Text, TextBox2.Text, TextBox3.Text, TextBox4.Text);
        Response.Write("<script>alert('添加成功！')</script>");

    }
}