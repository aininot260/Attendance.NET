﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using QCloud.CosApi.Common;
using QCloud.CosApi.Util;
using QCloud.CosApi.Api;
public partial class student_add : System.Web.UI.Page
{
    DBOP Query = new DBOP();
    //COS
    const int APP_ID = 1252885834;
    const string SECRET_ID = "AKIDfFgyABujEXecT8bt1xqX4QXTDl9LnWaT";
    const string SECRET_KEY = "qJhtfLsFWcZekfO1Shk7Og1HMFwRktwo";
    const string bucketName = "attendance";
    protected void Page_Load(object sender, EventArgs e)
    {
        List<string> obj = new List<string>();
        for (int i = 0; i < 17; i++)
        {
            obj.Add("");
        }
        Repeater1.DataSource = obj;
        Repeater1.DataBind();
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem Item in Repeater1.Items)
        {
            Boolean fileOk = false;
            TextBox tbId = (TextBox)Item.FindControl("TextBox1");
            TextBox tbName = (TextBox)Item.FindControl("TextBox2");
            DropDownList ddClass = (DropDownList)Item.FindControl("DropDownList1");
            if (((FileUpload)Item.FindControl("pic_upload")).HasFile)//验证是否包含文件
            {
                //取得文件的扩展名,并转换成小写
                string fileExtension = Path.GetExtension(((FileUpload)Item.FindControl("pic_upload")).FileName).ToLower();
                //验证上传文件是否图片格式
                fileOk = IsImage(fileExtension);
                if (fileOk)
                {
                    if (((FileUpload)Item.FindControl("pic_upload")).PostedFile.ContentLength < 8192000)
                    {
                        Query.student_add(Request.Form[tbId.UniqueID], Request.Form[tbName.UniqueID], Request.Form[ddClass.UniqueID]);
                        var cos = new CosCloud(APP_ID, SECRET_ID, SECRET_KEY);
                        var result = "";
                        string t1 = Request.Form[tbId.UniqueID][0].ToString() + Request.Form[tbId.UniqueID][1].ToString();
                        string t2 = Request.Form[tbId.UniqueID][4].ToString() + Request.Form[tbId.UniqueID][5].ToString() + Request.Form[tbId.UniqueID][6].ToString() + Request.Form[tbId.UniqueID][7].ToString();
                        string remotePath = "/" + t1 + "/" + t2 + "/" + Request.Form[tbId.UniqueID] + ".jpg";
                        string filepath = "/images/";
                        if (Directory.Exists(Server.MapPath(filepath)) == false)//如果不存在就创建file文件夹
                        {
                            Directory.CreateDirectory(Server.MapPath(filepath));
                        }
                        string virpath = filepath + ((FileUpload)Item.FindControl("pic_upload")).FileName;//这是存到服务器上的虚拟路径
                        string mappath = Server.MapPath(virpath);//转换成服务器上的物理路径
                        ((FileUpload)Item.FindControl("pic_upload")).PostedFile.SaveAs(mappath);//保存图片       
                        var uploadParasDic = new Dictionary<string, string>();
                        uploadParasDic.Add(CosParameters.PARA_BIZ_ATTR, "");
                        uploadParasDic.Add(CosParameters.PARA_INSERT_ONLY, "0");
                        //uploadParasDic.Add(CosParameters.PARA_SLICE_SIZE,SLICE_SIZE.SLIZE_SIZE_3M.ToString());             
                        result = cos.UploadFile(bucketName, remotePath, mappath, uploadParasDic);
                        ((Label)Item.FindControl("lbl_pic")).Text = "";
                    }
                    else
                    {
                        ((Label)Item.FindControl("lbl_pic")).Text = "文件大小超出8M！请重新选择！";
                    }
                }
                else
                {
                    ((Label)Item.FindControl("lbl_pic")).Text = "要上传的文件类型不对！请重新选择！";
                }
            }
            else
            {
                ((Label)Item.FindControl("lbl_pic")).Text = "请选择要上传的图片！";
            }
        }
        Response.Write("<script>alert('添加成功！')</script>");
    }
    public bool IsImage(string str)
    {
        bool isimage = false;
        string thestr = str.ToLower();
        //限定只能上传jpg和gif图片
        string[] allowExtension = { ".jpg", ".gif", ".bmp", ".png" };
        //对上传的文件的类型进行一个个匹对
        for (int i = 0; i < allowExtension.Length; i++)
        {
            if (thestr == allowExtension[i])
            {
                isimage = true;
                break;
            }
        }
        return isimage;
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("Student_add.aspx");
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("Student_edit.aspx");
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("Student_del.aspx");
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        Response.Redirect("Student_info.aspx");
    }
}