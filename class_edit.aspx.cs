﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class class_edit : System.Web.UI.Page
{
    DBOP Query = new DBOP();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("class_add.aspx");
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("class_edit.aspx");
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("class_del.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("class_info.aspx");
    }

    protected void Button6_Click(object sender, EventArgs e)
    {
        Query.class_edit(TextBox5.Text, TextBox6.Text, TextBox7.Text);
        Response.Write("<script>alert('修改成功！')</script>");
    }
}